// 达人关系管理系统子路由
export default [
  {
    path: '/index/page1', 
    name: 'page1',
    component: () => import('@/views/index/Page1'),
    meta: {

    },
  },
  {
    path: '/index/page2', 
    name: 'page2',
    component: () => import('@/views/index/Page2'),
    meta: {

    },
  },
  {
    path: '/index/page3', 
    name: 'page3',
    component: () => import('@/views/index/Page3'),
    meta: {

    },
  },
  {
    path: '/index/page4', 
    name: 'page4',
    component: () => import('@/views/index/Page4'),
    meta: {

    },
  },
  {
    path: '/index/page5', 
    name: 'page5',
    component: () => import('@/views/index/Page5'),
    meta: {

    },
  },
  {
    path: '/index/page6', 
    name: 'page6',
    component: () => import('@/views/index/Page6'),
    meta: {

    },
  },
]