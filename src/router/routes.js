import components from './modules/components'
import relationships from './modules/relationships'

const routes = [
    {path: '/', redirect: '/entry'},
    {path: '/login', name: 'login', component: () => import('@/views/login/Login')},
    {path: '/entry', name: 'entry',  component: () => import('@/views/entry/Entry')},
    {path: '/:catchAll(.*)', name: '404',  component: () => import('@/views/404/404')},
    {
      // index: 每个子系统公用主页面（包含左侧菜单栏，右侧用户头部，右侧内容区....，子路由根据各子系统菜单数据配置展示，所以无固定默认路由展示）
      path: '/index', 
      name: 'index',
      component: () => import('@/views/index/Index'),
      children: [
        // 组件
        ...components,
        // 达人关系管理系统子路由
        ...relationships,
        // 项目管理系统子路由

        // 运营支持管理系统子路由

      ]
    },
  ] 
  
export default routes