// 左侧导航栏菜单数据配置
export default [
  {
    icon: "icon-chubeiku",
    label: "页面3",
    routeName: "page3",
  },
  {
    icon: "icon-chubeiku",
    label: "示例导航", // 必填：菜单展示名称、面包屑展示名称、路由展示名称
    routeName: "demoNav", // 必填：面包屑标识、路由跳转name,必须与路由配置里的路由名称/name一致
    children: [
      {
        icon: "icon-baobiao",
        label: "子导航1",
        routeName: "sub1",
        // 路由参数
        routeQuery: {
          page: 1
        },
        children: [
          {
            icon: "icon-baobiao",
            label: "页面1",
            routeName: "page1",
          },
          {
            icon: "icon-baobiao",
            label: "页面2",
            routeName: "page7",
            children: [
              {
                icon: "icon-baobiao",
                label: "页面4",
                routeName: "page4",
              },
              {
                icon: "icon-baobiao",
                label: "页面5",
                routeName: "page5",
              }
            ]
          }
        ]
      },
      {
        icon: "icon-baobiao",
        label: "子导航2",
        routeName: "page6"
      }
    ]
  },
  {
    icon: "icon-chubeiku",
    label: "组件",
    routeName: "components",
    children: [
      {
        icon: "icon-baobiao",
        label: "数据搜索栏",
        routeName: "searchBar",
      },
      {
        icon: "icon-baobiao",
        label: "表格",
        routeName: "tablePlus",
      },
      
    ],
  }
];